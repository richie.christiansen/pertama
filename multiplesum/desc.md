## Multiple Sum

### Deskripsi
Pak Dengklek memiliki sebuah *array A*  berisi *N* buah bilangan bulat. Anda harus menghitung jumlah dari seluruh elemen dari *array* tersebut.

### Masukkan
<pre>
N
A<sub>1</sub> A<sub>2</sub> ... A<sub>N</sub>
</pre>

### Keluaran
Sebuah baris berisi jumlah dari *array* tersebut (A<sub>1</sub>+A<sub>2</sub>+...+A<sub>N</sub>)

### Contoh Masukan 1

~~~
5
1 2 3 4 5
~~~

### Contoh Keluaran 1
~~~
15
~~~

### Penjelasan
Pada contoh pertama, nilai jumlah dari *array* tersebut adalah (1+2+3+4+5)

### Batasan
$1 \leq N \leq 100$

$1 \leq$ A<sub>i</sub> $\leq 100$
