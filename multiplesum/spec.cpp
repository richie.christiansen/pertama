#include <tcframe/spec.hpp>
#include<bits/stdc++.h>
using namespace std;
using namespace tcframe;

class ProblemSpec : public BaseProblemSpec {
protected:
    int N;
    vector<int> arr;
    int sum;
    void InputFormat() {
        LINE(N);
        LINE(arr % SIZE(N));
    }

    void OutputFormat() {
        LINE(sum);
    }

    void GradingConfig() {
        TimeLimit(1);
        MemoryLimit(64);
    }

    void Constraints() {
        CONS(1 <= N && N <= 1000);
        CONS(each(arr,1,1000));
    }
private :
	bool each(const vector<int>& X,int lo,int high)
	{
		for(int x:X)
		{
			if(x < lo || x >high)
			{
				return false;
			}
		}
		return true;
	}
};

class TestSpec : public BaseTestSpec<ProblemSpec> {
protected:
	
    void SampleTestCase1() {
        Input({
            "5",
            "1 2 3 4 5"
        });
        Output({
            "15"
        });
    }
	void BeforeTestCase(){
		arr.clear();
	}
    void TestCases() {
        CASE(N = 4, arr = {1,2,3,4});
        CASE(N = 1000,maks());
        CASE(N = 5, arr = {1,1,1,1,1});
        CASE(N = rnd.nextInt(1,1000),randomarray());
    }
private:
	void randomarray()
	{
		for(int i = 0;i < N;i++)
		{
			arr.push_back(rnd.nextInt(1,1000));
		}
	}
	void maks()
	{
		for(int i = 0;i < N;i++)
		{
			arr.push_back(1000);
		}
	}
};
